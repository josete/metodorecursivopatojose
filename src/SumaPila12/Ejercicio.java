/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SumaPila12;

import java.math.BigDecimal;
import java.util.Stack;

/**
 *
 * @author PC-Portatil
 */
public class Ejercicio {

    BigDecimal suma = BigDecimal.ZERO;

    public BigDecimal sumaRecursivaElementosPila(Stack pila) {

        if (pila.empty()) {
            return suma;
        } else {
            suma = suma.add(new BigDecimal(pila.peek().toString()));
            pila.pop();
            return sumaRecursivaElementosPila(pila);
        }
    }

}
