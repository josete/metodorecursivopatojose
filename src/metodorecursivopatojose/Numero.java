/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package metodorecursivopatojose;

/**
 *
 * @author PC-Portatil
 */
public class Numero {
    
    public static int sumaNPrimeros(int i){
        if(i<0) throw new ArithmeticException("Este metodo no permite numeros negativos");
        if(i == 0) return 0;
        else return i + sumaNPrimeros(i-1);
    }
}
