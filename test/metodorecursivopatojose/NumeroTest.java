/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package metodorecursivopatojose;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author PC-Portatil
 */
public class NumeroTest {
    
    public NumeroTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSumaHastaCero(){
        assertEquals(0, Numero.sumaNPrimeros(0));
    }
    
    @Test
    public void testSumaHasta3(){
        assertEquals(6, Numero.sumaNPrimeros(3));
    }
    
    @Test(expected = ArithmeticException.class)
    public void testSumaHastaMenos3() throws ArithmeticException{
        Numero.sumaNPrimeros(-3);
    }
    
    @Test
    public void testHasta10000() throws ArithmeticException{
        assertEquals(50005000, Numero.sumaNPrimeros(10000));
    }
    
}
