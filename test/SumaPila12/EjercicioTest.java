/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package SumaPila12;

import java.math.BigDecimal;
import java.util.Stack;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author PC-Portatil
 */
public class EjercicioTest {
    
    public EjercicioTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSumaRecursivaElementosPila(){
        Ejercicio ejer = new Ejercicio();
        Stack pila = new Stack();
        pila.push(3);
        pila.push(4);
        BigDecimal bg = ejer.sumaRecursivaElementosPila(pila);
        BigDecimal resultadoEsperado = new BigDecimal("7");
        assertEquals(resultadoEsperado, bg);
    }
    
    @Test
    public void testSumaRecursivaPilaVacia(){
        Ejercicio ejer = new Ejercicio();
        Stack pila = new Stack();
        BigDecimal bg = ejer.sumaRecursivaElementosPila(pila);
        BigDecimal resultadoEsperado = BigDecimal.ZERO;
        assertEquals(resultadoEsperado, bg);
    }
    
    @Test
    public void testSumaRecursiva100(){
        Ejercicio ejer = new Ejercicio();
        Stack pila = new Stack();
        for(int i=0;i<=100;i++){
            pila.push(i);
        }
        BigDecimal bg = ejer.sumaRecursivaElementosPila(pila);
        BigDecimal resultadoEsperado = new BigDecimal("5050");
        assertEquals(resultadoEsperado, bg);
    }
    
}
